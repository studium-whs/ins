<?php
require_once('./domain/Form.php');

$form = validateFormInput();

generateResponseBasedOnFormInput($form);

$form->exportFormToXml();

function validateFormInput(): Form
{

    $address = '';
    $firstName = '';
    $lastName = '';
    $age = -1;
    $sendCopyToMail = 0;
    $callbacks = 0;
    $emailAddress = '';
    $phoneNumber = '';
    $revisit = 0;
    $improvements = '';
    $url = '';
    $attentive = '';
    $rating_content = -1;
    $rating_appearance = -1;
    $password = '';

    $spamProtectionHash = password_hash('Internetsprachen', PASSWORD_BCRYPT);


    if (isset($_POST['password']) && $_POST['password'] != null && !empty(trim($_POST['password']))) {
        if (password_verify($_POST['password'], $spamProtectionHash)) {
            $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
        } else {
            trigger_error('Falsches Passwort! Bitte überprüfen Sie Ihre eingabe im Passwort Feld.', E_USER_ERROR);
        }
    } else {
        trigger_error('Bitte füllen Sie das Passwort Feld aus.', E_USER_ERROR);
    }


    if (isset($_POST['address']) && $_POST['address'] != null && !empty(trim($_POST['address']))) {
        $address = $_POST['address'];
    }

    if (isset($_POST['firstName']) && $_POST['firstName'] != null && !empty(trim($_POST['firstName']))) {
        $firstName = $_POST['firstName'];
    }

    if (isset($_POST['lastName']) && $_POST['lastName'] != null && !empty(trim($_POST['lastName']))) {
        $lastName = $_POST['lastName'];
    }

    if (isset($_POST['age']) && $_POST['age'] != null && $_POST['age'] > 18 && $_POST['age'] < 99) {
        $age = $_POST['age'];
    }

    if (isset($_POST['sendCopyToMail']) && !empty($_POST['sendCopyToMail'])) {
        $sendCopyToMail = $_POST['sendCopyToMail'];
    }

    if (isset($_POST['callbacks']) && $_POST['callbacks'] != null) {
        $callbacks = $_POST['callbacks'];
    }

    if ($sendCopyToMail) {
        if (isset($_POST['emailAddress']) && ($_POST['emailAddress'] != null && !empty(trim($_POST['emailAddress'])))) {
            $emailAddress = $_POST['emailAddress'];
        } else {
            trigger_error('Bitte geben Sie eine E-Mail Adresse an.', E_USER_WARNING);
        }
    }

    if ($callbacks) {
        if (isset($_POST['emailAddress']) && ($_POST['emailAddress'] != null && !empty(trim($_POST['emailAddress'])))) {
            $emailAddress = $_POST['emailAddress'];
        }

        if (isset($_POST['phoneNumber']) && ($_POST['phoneNumber'] != null && !empty(trim($_POST['phoneNumber'])))) {
            $phoneNumber = $_POST['phoneNumber'];
        }

        if (isset($_POST['url']) && ($_POST['url'] != null && !empty(trim($_POST['url'])))) {
            $url = $_POST['url'];
        }

        if (empty($emailAddress) && empty($phoneNumber) && empty($url)) {
            trigger_error('Bitte geben Sie eine der folgenden Kontaktmöglichkeiten an: Email, Telefon, URL.', E_USER_WARNING);
        }
    }

    if (isset($_POST['revisit']) && $_POST['revisit'] != null) {
        $revisit = $_POST['revisit'];
    }

    if (isset($_POST['improvements']) && $_POST['improvements'] != null && !empty(trim($_POST['improvements']))) {
        $improvements = $_POST['improvements'];
    }

    if (isset($_POST['attentive']) && $_POST['attentive'] != null && !empty(trim($_POST['attentive']))) {
        $attentive = $_POST['attentive'];
    }

    if (isset($_POST['rating_content']) && $_POST['rating_content'] != null && ($_POST['rating_content'] >= 1 && $_POST['rating_content'] <= 6)) {
        $rating_content = $_POST['rating_content'];
    }

    if (isset($_POST['rating_appearance']) && $_POST['rating_appearance'] != null && ($_POST['rating_appearance'] >= 1 && $_POST['rating_appearance'] <= 6)) {
        $rating_appearance = $_POST['rating_appearance'];
    }

    return new Form($address, $firstName, $lastName, $age, $sendCopyToMail, $callbacks, $emailAddress, $phoneNumber,
        $revisit, $improvements, $url, $attentive, $rating_content, $rating_appearance, $password);

//    /* left here for debugging purposes */
//    $form = new Form($address, $firstName, $lastName, $age, $sendCopyToMail, $callbacks, $emailAddress, $phoneNumber,
//        $revisit, $improvements, $url, $attentive, $rating_content, $rating_appearance, $password);
//
//    echo $form;
//
//    return $form;
}


function generateResponseBasedOnFormInput($form)
{
    echo '<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Danke</title>
    <link href="../css/localStyling.css" rel="stylesheet">
    <link href="../css/globalStyling.css" rel="stylesheet">
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css2?family=Goldman&amp;display=swap" rel="stylesheet">
</head>
<body>
<h1>Danke für Ihr Feedback</h1>';

echo '<p class="p_center">Feedback angenommen am: ' . dateTimeInformation() . '</p>';

echo '<hr class="hr"/>

<div class="div_wrapper">
    <div class="div_nav">
        <nav>
            <ul>
                <li>
                    <a href="../html/application/home.html">Home</a>
                </li>
                <li>
                    <a href="../html/application/application.html">Bewerbung</a>
                </li>
                <li>
                    <a href="../html/application/resume.html">Lebenslauf</a>
                </li>
                <li>
                    <a href="../html/application/jobs.html">Berufliche Tätigkeiten</a>
                </li>
                <li>
                    <a href="../html/application/technical_knowledge.html">Fachliche Kenntnisse</a>
                </li>
                <li>
                    <a href="../html/application/hobbies.html">Hobbies</a>
                </li>
                <li>
                    <a href="../html/application/forms.html">Formulare</a>
                </li>
                <li>
                    <a href="../html/application/contact.html">Kontakt</a>
                </li>
                <li>
                    <a href="../html/application/feedback.html">Feedback</a>
                </li>
            </ul>
        </nav>
    </div>';

    echo '<div class="div_inner_wrapper">
            <div class="div_responsive">';
    
    echo '<p>' . greetingMessage($form) . '</p>';
    echo '<br>';
    echo '<p>' . contentRatingMessage($form) . '<br>';
    echo '<p>' . appearanceRatingMessage($form);

    echo '</div>
    </div>
</div>
</body>
</html>';
}


function greetingMessage($form): string
{
    return 'Hallo ' . $form->getFormOfAddress() . ' ' . $form->getFirstName() . ' ' . $form->getLastName()
        . ' vielen Dank, dass Sie sich die Zeit genommen haben Ihr Feedback mit mir zu teilen.';
}

function contentRatingMessage($form): string
{
    $contentResponse = '';

    if ($form->getContentRating() < 3 && $form->getContentRating() > 0) {
        $contentResponse = 'Es freut mich, dass Sie durch meine Webseite erfahren konnten was Sie über mich wissen wollten.';
    } else if ($form->getContentRating() === 3) {
        $contentResponse = 'Ich werde versuchen in künftigen Updates mehr Wert auf den Inhalt der Seite zu legen.';
    } else if ($form->getContentRating() > 3 && $form->getContentRating() < 7) {
        $contentResponse = 'Ich bedauere, dass Sie keine für sich relevanten Informationen auf dieser Seite gefunden haben.';
        if ($form->getImprovements() != null && !empty(trim($form->getImprovements()))) {
            $contentResponse = $contentResponse . 'Ich werde für zukünftige Updates folgende Ratschläge aus Ihren 
                Verbesserungsvorschlägen berücksichtigen: <br></p><hr><pre>' . $form->getImprovements() . '</pre><hr>';
        } else {
            $contentResponse = $contentResponse . '</p>';
        }
    } else if ($form->getContentRating() < 1 || $form->getContentRating() > 6) {
        $contentResponse = 'Sie haben keine Bewertung über den Inhalt meiner Website hinterlassen.';
    }
    return $contentResponse;
}


function appearanceRatingMessage($form): string
{

    return match ($form->getAppearanceRating()) {
        1 => 'Es freut mich, dass die Gestaltung meiner Website genau Ihren Geschmack trifft.',
        2 => 'Es freut mich, dass Ihnen die Gestaltung meiner Website gefällt.',
        3 => 'Ich hoffe, Sie haben Verbesserungsvorschläge hinterlassen, um meine Webseite vom Durchschnitt abzuheben.',
        4 => 'Schade, dass Sie das Design der Webseite nur unterdurchschnittlich finden.',
        5 => 'Ich bedauere, dass Sie mit dem Design meiner Webseite nichts anfangen können.',
        6 => 'Ich möchte mich bei Ihnen entschuldigen, Ihren Augen solch eine Zumutung präsentiert zu haben.',
        default => 'Sie haben keine Bewertung zum Design meiner Website hinterlassen.',
    };
}

function dateTimeInformation(): string
{
    date_default_timezone_set('Europe/Berlin');

    return strftime('%d-%m-%Y %H-%M-%S');
}
