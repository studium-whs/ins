<?php
require_once('./domain/FeedbackDatabase.php');
require_once('./domain/Feedback.php');
require_once('./domain/Visitor.php');
require_once('./domain/Rating.php');
require_once('./domain/Info.php');
require_once('./domain/Contact.php');


const HTML_TABLE_DATA_START = '<td>';
const HTML_TABLE_DATA_END = '</td>';

importFeedbackDatabase();

$feedbackDatabase = importFeedbackDatabase();

generateFeedbackDatabaseTable();


function importFeedbackDatabase(): FeedbackDatabase
{
    $xml = simplexml_load_file("../xml/generated/accumulated/feedback_database.xml");

    $feedbackDatabase = new FeedbackDatabase();
    $feebackArray = [];

    for ($i = 0; $i < $xml->children()->count(); $i++) {
        $feedback = new Feedback();

        $visitor = new Visitor();
        $contact = new Contact();
        $rating = new Rating();
        $info = new Info();


        /* Set visitor information */
        $visitor->setFormOfAddress($xml->feedback[$i]->besucher['anrede']);
        $visitor->setFirstName($xml->feedback[$i]->besucher['vorname']);
        $visitor->setLastName($xml->feedback[$i]->besucher['nachname']);
        $visitor->setAge((int)$xml->feedback[$i]->besucher->alter);

        /* Set contatct information */
        $contact->setCallbacks((bool)$xml->feedback[$i]->besucher->kontakt['rueckfrage_erlaubt']);
        $contact->setEmailAddress($xml->feedback[$i]->besucher->kontakt->emailadresse);
        $contact->setUrl($xml->feedback[$i]->besucher->kontakt->website);
        $contact->setPhoneNumber($xml->feedback[$i]->besucher->kontakt->telefonnummer);

        /* Set rating information */
        $rating->setRevisit($xml->feedback[$i]->bewertung['erneuter_besuch'] == 'ja' ? true : false);
        $rating->setContentRating($xml->feedback[$i]->bewertung['note_inhalt']);
        $rating->setAppearanceRating((int)$xml->feedback[$i]->bewertung['note_aussehen']);
        $rating->setImprovements($xml->feedback[$i]->bewertung->vorschlag);

        /* Set info information */
        $info->setSendCopyPerMail((bool)$xml->feedback[$i]->info->{'email-gesendet'} == 'ja' ? true : false);
        $info->setDate($xml->feedback[$i]->info->datum);
        $info->setTime($xml->feedback[$i]->info->uhrzeit);


        /* Join objects together */
        $visitor->setContact($contact);
        $feedback->setVisitor($visitor);
        $feedback->setRating($rating);
        $feedback->setInfo($info);

        $feebackArray[$i] = $feedback;


    }

    $feedbackDatabase->setFeedback($feebackArray);

    return $feedbackDatabase;
}

function generateFeedbackDatabaseTable()
{
    echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Auswertung</title>
    <link href="../css/feedbackDatabaseTable.css" rel="stylesheet">
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css2?family=Goldman&display=swap" rel="stylesheet">
</head>
<body>
<div class="div_responsive_big">
    <table>
        <caption>
            <h2>Auswertung</h2>
        </caption>
        <tr>
            <th colspan="16" scope="col"><h3><strong>Feedback Datenbank</strong></h3></th>
        </tr>
        <tr>
            <th colspan="1" rowspan="3" scope="col" class="th_number">Feedback</th>
            <th colspan="8" scope="col">Besucher</th>
            <th colspan="4" scope="col">Bewertung</th>
            <th colspan="3" scope="col">Info</th>
        <tr>
            <th colspan="1" rowspan="2" scope="col" class="th_string_small">Anrede</th>
            <th colspan="1" rowspan="2" scope="col" class="th_string_small">Vorname</th>
            <th colspan="1" rowspan="2" scope="col" class="th_string_small">Nachname</th>
            <th colspan="1" rowspan="2" scope="col" class="th_number">Alter</th>
            <th colspan="4" scope="col">Kontakt</th>
            <th colspan="1" rowspan="2" scope="col" class="th_boolean">Erneuter Besuch</th>
            <th colspan="1" rowspan="2" scope="col" class="th_number">Bewertung Inhalt</th>
            <th colspan="1" rowspan="2" scope="col" class="th_string_small">Bewertung Aussehen</th>
            <th colspan="1" rowspan="2" scope="col" class="th_string_big">Verbesserungen</th>
            <th colspan="1" rowspan="2" scope="col" class="th_boolean">Email versendet</th>
            <th colspan="1" rowspan="2" scope="col" class="th_number">Datum</th>
            <th colspan="1" rowspan="2" scope="col" class="th_number">Uhrzeit</th>
        <tr>
            <th colspan="1" scope="col" class="th_boolean">Rückfragen</th>
            <th colspan="1" scope="col" class="th_string_big">Email</th>
            <th colspan="1" scope="col" class="th_string_big">Website</th>
            <th colspan="1" scope="col" class="th_string_small">Telefonnummer</th>
        </tr>';

    echo createTableData();

    echo '</table>
</div>
</body>
</html>';
}


function createTableData(): string
{
    $feedbackDatabaseTableData = '';

    global $feedbackDatabase;

    for ($i = 0; $i < count($feedbackDatabase->getFeedback()); $i++) {

        $improvements = str_replace("<![CDATA[", "", $feedbackDatabase->getFeedback()[$i]->getRating()->getImprovements());
        $improvements = str_replace("]]>", "", $improvements);

        $feedbackDatabaseTableData = $feedbackDatabaseTableData
            . '<tr>'
            . HTML_TABLE_DATA_START . $i . HTML_TABLE_DATA_END
            . HTML_TABLE_DATA_START . $feedbackDatabase->getFeedback()[$i]->getVisitor()->getFormOfAddress() . HTML_TABLE_DATA_END
            . HTML_TABLE_DATA_START . $feedbackDatabase->getFeedback()[$i]->getVisitor()->getFirstName() . HTML_TABLE_DATA_END
            . HTML_TABLE_DATA_START . $feedbackDatabase->getFeedback()[$i]->getVisitor()->getLastName() . HTML_TABLE_DATA_END
            . HTML_TABLE_DATA_START . $feedbackDatabase->getFeedback()[$i]->getVisitor()->getAge() . HTML_TABLE_DATA_END
            . HTML_TABLE_DATA_START . $feedbackDatabase->getFeedback()[$i]->getVisitor()->getContact()->isCallbacks() . HTML_TABLE_DATA_END
            . HTML_TABLE_DATA_START . $feedbackDatabase->getFeedback()[$i]->getVisitor()->getContact()->getEmailAddress() . HTML_TABLE_DATA_END
            . HTML_TABLE_DATA_START . $feedbackDatabase->getFeedback()[$i]->getVisitor()->getContact()->getUrl() . HTML_TABLE_DATA_END
            . HTML_TABLE_DATA_START . $feedbackDatabase->getFeedback()[$i]->getVisitor()->getContact()->getPhoneNumber() . HTML_TABLE_DATA_END
            . HTML_TABLE_DATA_START . $feedbackDatabase->getFeedback()[$i]->getRating()->isRevisit() . HTML_TABLE_DATA_END
            . HTML_TABLE_DATA_START . $feedbackDatabase->getFeedback()[$i]->getRating()->getContentRating() . HTML_TABLE_DATA_END
            . HTML_TABLE_DATA_START . $feedbackDatabase->getFeedback()[$i]->getRating()->getAppearanceRating() . HTML_TABLE_DATA_END
            . HTML_TABLE_DATA_START . $improvements . HTML_TABLE_DATA_END
            . HTML_TABLE_DATA_START . $feedbackDatabase->getFeedback()[$i]->getInfo()->isSendCopyPerMail() . HTML_TABLE_DATA_END
            . HTML_TABLE_DATA_START . $feedbackDatabase->getFeedback()[$i]->getInfo()->getDate() . HTML_TABLE_DATA_END
            . HTML_TABLE_DATA_START . $feedbackDatabase->getFeedback()[$i]->getInfo()->getTime() . HTML_TABLE_DATA_END
            . '</tr>';
    }

    return $feedbackDatabaseTableData;
}