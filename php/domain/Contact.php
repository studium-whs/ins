<?php


class Contact
{
    const HTML_BREAK = '</br>';
    
    private bool $callbacks;
    private String $emailAddress;
    private String $url;
    private String $phoneNumber;

    /**
     * @return bool
     */
    public function isCallbacks(): bool
    {
        return $this->callbacks;
    }

    /**
     * @param bool $callbacks
     */
    public function setCallbacks(bool $callbacks): void
    {
        $this->callbacks = $callbacks;
    }

    /**
     * @return String
     */
    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    /**
     * @param String $emailAddress
     */
    public function setEmailAddress(string $emailAddress): void
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return String
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param String $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return String
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @param String $phoneNumber
     */
    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function __toString(): string
    {
        return 'Callbacks: ' . $this->callbacks . HTML_BREAK
            . 'EmailAddress: ' . $this->emailAddress . HTML_BREAK
            . 'URL: ' . $this->url . HTML_BREAK
            . 'PhoneNumber: ' . $this->phoneNumber . HTML_BREAK;
    }


}