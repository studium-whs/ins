<?php

class Form
{
    private string $formOfAddress;
    private string $firstName;
    private string $lastName;
    private int $age;
    private bool $sendCopyPerMail;
    private bool $callbacks;
    private string $emailAddress;
    private string $phoneNumber;
    private bool $revisit;
    private string $improvements;
    private string $url;
    private string $attentive;
    private int $contentRating;
    private int $appearanceRating;
    private string $password;

    /**
     * Form constructor.
     * @param string $formOfAddress
     * @param string $firstName
     * @param string $lastName
     * @param int $age
     * @param bool $sendCopyPerMail
     * @param bool $callbacks
     * @param string $emailAddress
     * @param string $phoneNumber
     * @param bool $revisit
     * @param string $improvements
     * @param string $url
     * @param string $attentive
     * @param int $contentRating
     * @param int $appearanceRating
     * @param string $password
     */
    public function __construct(string $formOfAddress, string $firstName, string $lastName, int $age, bool $sendCopyPerMail, bool $callbacks, string $emailAddress, string $phoneNumber, bool $revisit, string $improvements, string $url, string $attentive, int $contentRating, int $appearanceRating, string $password)
    {
        $this->formOfAddress = $formOfAddress;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->age = $age;
        $this->sendCopyPerMail = $sendCopyPerMail;
        $this->callbacks = $callbacks;
        $this->emailAddress = $emailAddress;
        $this->phoneNumber = $phoneNumber;
        $this->revisit = $revisit;
        $this->improvements = $improvements;
        $this->url = $url;
        $this->attentive = $attentive;
        $this->contentRating = $contentRating;
        $this->appearanceRating = $appearanceRating;
        $this->password = $password;
    }

    public function __toString(): string
    {
        return
            "Form of address: " . $this->formOfAddress
            . "<br>"
            . "First name: " . $this->firstName
            . "<br>"
            . "Last name: " . $this->lastName
            . "<br>"
            . "Age: " . strval($this->age)
            . "<br>"
            . "Send copy to mail: " . strval($this->sendCopyPerMail)
            . "<br>"
            . "Callback possible: " . strval($this->callbacks)
            . "<br>"
            . "Email address: " . $this->emailAddress
            . "<br>"
            . "Phone number: " . $this->phoneNumber
            . "<br>"
            . "Would revisit: " . strval($this->revisit)
            . "<br>"
            . "Improvements: " . $this->improvements
            . "<br>"
            . "URL: " . $this->url
            . "<br>"
            . "Attentive: " . $this->attentive
            . "<br>"
            . "Content rating: " . strval($this->contentRating)
            . "<br>"
            . "Appearance rating: " . strval($this->appearanceRating)
            . "<br>"
            . "Password: " . $this->password;
    }

    public function exportFormToXml(): void
    {
        $xmlTemplate = simplexml_load_file('../xml/template/xmlFeedbackTemplate.xml');


        $personalInformation = $xmlTemplate->personalInformation;

        $personalInformation->formOfAddress = $this->formOfAddress;
        $personalInformation->firstName = $this->firstName;
        $personalInformation->lastName = $this->lastName;
        $personalInformation->age = $this->age;
        $personalInformation->emailAddress = $this->emailAddress;
        $personalInformation->phoneNumber = $this->phoneNumber;
        $personalInformation->url = $this->url;


        $feedBackDetails = $xmlTemplate->feedbackDetails;

        $feedBackDetails->revisit = $this->revisit;
        $feedBackDetails->improvements = '<![CDATA[' . $this->improvements . ']]>';
        $feedBackDetails->attentive = $this->attentive;
        $feedBackDetails->contentRating = $this->contentRating;
        $feedBackDetails->appearanceRating = $this->appearanceRating;


        $other = $xmlTemplate->other;

        $other->sendCopyPerMail = $this->sendCopyPerMail;
        $other->callbacks = $this->callbacks;
        $other->password = $this->password;

        $dateTime = new DateTime();

        $other->creationTimestamp = $dateTime->format('c');

        $dom = dom_import_simplexml($xmlTemplate)->ownerDocument;
        $dom->formatOutput = true;
        $dom->save(realpath('../xml/generated/single') . DIRECTORY_SEPARATOR . $dateTime->format('Y-m-d_H-i-s-v') . '_' . uniqid('', true) . '.xml');
    }


    /**
     * @return string
     */
    public function getFormOfAddress(): string
    {
        return $this->formOfAddress;
    }

    /**
     * @param string $formOfAddress
     */
    public function setFormOfAddress(string $formOfAddress): void
    {
        $this->formOfAddress = $formOfAddress;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @param int $age
     */
    public function setAge(int $age): void
    {
        $this->age = $age;
    }

    /**
     * @return bool
     */
    public function isSendCopyPerMail(): bool
    {
        return $this->sendCopyPerMail;
    }

    /**
     * @param bool $sendCopyPerMail
     */
    public function setSendCopyPerMail(bool $sendCopyPerMail): void
    {
        $this->sendCopyPerMail = $sendCopyPerMail;
    }

    /**
     * @return bool
     */
    public function isCallbacks(): bool
    {
        return $this->callbacks;
    }

    /**
     * @param bool $callbacks
     */
    public function setCallbacks(bool $callbacks): void
    {
        $this->callbacks = $callbacks;
    }

    /**
     * @return string
     */
    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    /**
     * @param string $emailAddress
     */
    public function setEmailAddress(string $emailAddress): void
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return int
     */
    public function getPhoneNumber(): int
    {
        return $this->phoneNumber;
    }

    /**
     * @param int $phoneNumber
     */
    public function setPhoneNumber(int $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return bool
     */
    public function isRevisit(): bool
    {
        return $this->revisit;
    }

    /**
     * @param bool $revisit
     */
    public function setRevisit(bool $revisit): void
    {
        $this->revisit = $revisit;
    }

    /**
     * @return string
     */
    public function getImprovements(): string
    {
        return $this->improvements;
    }

    /**
     * @param string $improvements
     */
    public function setImprovements(string $improvements): void
    {
        $this->improvements = $improvements;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getAttentive(): string
    {
        return $this->attentive;
    }

    /**
     * @param string $attentive
     */
    public function setAttentive(string $attentive): void
    {
        $this->attentive = $attentive;
    }

    /**
     * @return int
     */
    public function getContentRating(): int
    {
        return $this->contentRating;
    }

    /**
     * @param int $contentRating
     */
    public function setContentRating(int $contentRating): void
    {
        $this->contentRating = $contentRating;
    }

    /**
     * @return int
     */
    public function getAppearanceRating(): int
    {
        return $this->appearanceRating;
    }

    /**
     * @param int $appearanceRating
     */
    public function setAppearanceRating(int $appearanceRating): void
    {
        $this->appearanceRating = $appearanceRating;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

}