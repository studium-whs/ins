<?php


class Visitor
{
    const HTML_BREAK = '</br>';
    
    private String $formOfAddress;
    private String $firstName;
    private String $lastName;
    private int $age;
    private Contact $contact;

    /**
     * @return String
     */
    public function getFormOfAddress(): string
    {
        return $this->formOfAddress;
    }

    /**
     * @param String $formOfAddress
     */
    public function setFormOfAddress(string $formOfAddress): void
    {
        $this->formOfAddress = $formOfAddress;
    }

    /**
     * @return String
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param String $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return String
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param String $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @param int $age
     */
    public function setAge(int $age): void
    {
        $this->age = $age;
    }

    /**
     * @return Contact
     */
    public function getContact(): Contact
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     */
    public function setContact(Contact $contact): void
    {
        $this->contact = $contact;
    }

    public function __toString(): string
    {
        return 'FromOfAddress: ' . $this->formOfAddress . HTML_BREAK
            . 'FirstName: ' . $this->firstName . HTML_BREAK
            . 'LastName: ' . $this->lastName . HTML_BREAK
            . 'Age: ' . $this->age . HTML_BREAK
            . 'Contact:</br> ' . $this->contact . HTML_BREAK;
    }


}