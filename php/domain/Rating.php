<?php


class Rating
{
    const HTML_BREAK = '</br>';
    
    private bool $revisit;
    private String $contentRating;
    private int $appearanceRating;
    private String $improvements;

    /**
     * @return bool
     */
    public function isRevisit(): bool
    {
        return $this->revisit;
    }

    /**
     * @param bool $revisit
     */
    public function setRevisit(bool $revisit): void
    {
        $this->revisit = $revisit;
    }

    /**
     * @return String
     */
    public function getContentRating(): string
    {
        return $this->contentRating;
    }

    /**
     * @param String $contentRating
     */
    public function setContentRating(string $contentRating): void
    {
        $this->contentRating = $contentRating;
    }

    /**
     * @return int
     */
    public function getAppearanceRating(): int
    {
        return $this->appearanceRating;
    }

    /**
     * @param int $appearanceRating
     */
    public function setAppearanceRating(int $appearanceRating): void
    {
        $this->appearanceRating = $appearanceRating;
    }


    /**
     * @return String
     */
    public function getImprovements(): string
    {
        return $this->improvements;
    }

    /**
     * @param String $improvements
     */
    public function setImprovements(string $improvements): void
    {
        $this->improvements = $improvements;
    }

    public function __toString(): string
    {
        return 'Revisit: ' . $this->revisit . HTML_BREAK
            . 'ContentRating: '. $this->contentRating . HTML_BREAK
            . 'AppearanceRating: ' . $this->appearanceRating . HTML_BREAK
            . 'Improvements: ' . $this->improvements . HTML_BREAK;
    }


}