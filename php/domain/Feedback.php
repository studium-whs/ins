<?php


class Feedback
{
    const HTML_BREAK = '</br>';
    
    private Visitor $visitor;
    private Rating $rating;
    private Info $info;

    /**
     * @return Visitor
     */
    public function getVisitor(): Visitor
    {
        return $this->visitor;
    }

    /**
     * @param Visitor $visitor
     */
    public function setVisitor(Visitor $visitor): void
    {
        $this->visitor = $visitor;
    }

    /**
     * @return Rating
     */
    public function getRating(): Rating
    {
        return $this->rating;
    }

    /**
     * @param Rating $rating
     */
    public function setRating(Rating $rating): void
    {
        $this->rating = $rating;
    }

    /**
     * @return Info
     */
    public function getInfo(): Info
    {
        return $this->info;
    }

    /**
     * @param Info $info
     */
    public function setInfo(Info $info): void
    {
        $this->info = $info;
    }

    public function __toString(): string
    {
        return 'Visitor:</br> ' . $this->visitor . HTML_BREAK
            . 'Rating:</br> ' . $this->rating . HTML_BREAK
            . 'Info:</br> ' . $this->info . HTML_BREAK;
    }


}