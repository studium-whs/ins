<?php


class Info
{
    const HTML_BREAK = '</br>';
    
    private bool $sendCopyPerMail;
    private String $date;
    private String $time;

    /**
     * @return bool
     */
    public function isSendCopyPerMail(): bool
    {
        return $this->sendCopyPerMail;
    }

    /**
     * @param bool $sendCopyPerMail
     */
    public function setSendCopyPerMail(bool $sendCopyPerMail): void
    {
        $this->sendCopyPerMail = $sendCopyPerMail;
    }

    /**
     * @return String
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param String $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return String
     */
    public function getTime(): string
    {
        return $this->time;
    }

    /**
     * @param String $time
     */
    public function setTime(string $time): void
    {
        $this->time = $time;
    }

    public function __toString(): string
    {
        return 'SendCopyPerMail: ' .  $this->sendCopyPerMail . HTML_BREAK
            . 'Date: ' . $this->date . HTML_BREAK
            . 'Time: ' . $this->time . HTML_BREAK;
    }


}