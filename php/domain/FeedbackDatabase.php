<?php


class FeedbackDatabase
{
    private $feedback;

    /**
     * FeedbackDatabase constructor.
     * @param $feedback
     */
    public function __construct()
    {
        $this->feedback = [];
    }

    /**
     * @return array
     */
    public function getFeedback(): array
    {
        return $this->feedback;
    }

    /**
     * @param array $feedback
     */
    public function setFeedback(array $feedback): void
    {
        $this->feedback = $feedback;
    }

    public function __toString(): string
    {
        return 'Feedback:</br> ' . implode(',', $this->feedback);
    }


}

