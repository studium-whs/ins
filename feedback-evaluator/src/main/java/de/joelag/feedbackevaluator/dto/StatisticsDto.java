package de.joelag.feedbackevaluator.dto;

import lombok.Data;

@Data
public class StatisticsDto {
    private double averageLayoutRating;
    private double revisitPercentage;
    private double mailPercentage;
}
