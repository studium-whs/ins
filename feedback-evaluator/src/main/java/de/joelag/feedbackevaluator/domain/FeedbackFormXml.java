package de.joelag.feedbackevaluator.domain;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "FEEDBACK_FORM_XML")
@Table(name = "FEEDBACK_FORM_XML")
@Data
public class FeedbackFormXml {

    @Id
    private Integer id;

    @Lob
    @Column(name = "XML_FILE")
    private byte[] xmlFile;

    @Column(name = "FILENAME")
    private String filename;

    @OneToOne
    @MapsId
    @JoinColumn(name = "ID")
    private FeedbackFormDetails feedbackFormDetails;

    @CreationTimestamp
    @Column(name = "CREATION_DATE")
    private LocalDateTime creationDate;

    @UpdateTimestamp
    @Column(name = "UPDATE_DATE")
    private LocalDateTime updateDate;
}
