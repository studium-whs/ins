package de.joelag.feedbackevaluator.domain;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "FEEDBACK_FORM_DETAILS")
@Table(name = "FEEDBACK_FORM_DETAILS")
@Data
public class FeedbackFormDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "FORM_OF_ADDRESS")
    private String formOfAddress;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "AGE")
    private Integer age;

    @Column(name = "EMAIL_ADDRESS")
    private String emailAddress;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "URL")
    private String url;

    @Column(name = "REVISIT")
    private Boolean revisit;

    @Column(name = "IMPROVEMENTS")
    @Lob
    private String improvements;

    @Column(name = "ATTENTIVE")
    private String attentive;

    @Column(name = "CONTENT_RATING")
    private Integer contentRating;

    @Column(name = "APPEARANCE_RATING")
    private Integer appearanceRating;

    @Column(name = "SEND_COPY_PER_MAIL")
    private Boolean sendCopyPerMail;

    @Column(name = "CALLBACKS")
    private Boolean callbacks;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "XML_CREATION_TIMESTAMP")
    private LocalDateTime xmlCreationTimestamp;

    @CreationTimestamp
    @Column(name = "CREATION_DATE")
    private LocalDateTime creationDate;

    @UpdateTimestamp
    @Column(name = "UPDATE_DATE")
    private LocalDateTime updateDate;
}
