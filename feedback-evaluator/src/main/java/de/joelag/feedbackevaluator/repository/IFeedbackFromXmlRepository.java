package de.joelag.feedbackevaluator.repository;

import de.joelag.feedbackevaluator.domain.FeedbackFormXml;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IFeedbackFromXmlRepository extends JpaRepository<FeedbackFormXml, Integer> {
}
