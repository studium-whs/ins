package de.joelag.feedbackevaluator.repository;

import de.joelag.feedbackevaluator.domain.FeedbackFormDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IFeedbackFormDetailRepository extends JpaRepository<FeedbackFormDetails, Integer> {
}
