package de.joelag.feedbackevaluator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeedbackEvaluatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(FeedbackEvaluatorApplication.class, args);
    }

}
