package de.joelag.feedbackevaluator.controller;

import de.joelag.feedbackevaluator.dto.StatisticsDto;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@RestController
@Produces("application/json")
@RequestMapping(value = "/feedback")
public interface IFeedbackController {

    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA)
    void uploadFeedbackForm(@RequestParam("multipartFile") MultipartFile multipartFile);

    @PostMapping(value = "/evaluateDirectory")
    void evaluateFormsInDirectory(@RequestParam("directory") String directory);

    @GetMapping(value = "/statistics")
    StatisticsDto getStatistics();

    @PostMapping(value = "/generateAccumulatedXml")
    void generateAccumulatedXml();

    @PostMapping(value = "/validateFeedbackDatabase")
    String validateFeedbackDatabase();
}
