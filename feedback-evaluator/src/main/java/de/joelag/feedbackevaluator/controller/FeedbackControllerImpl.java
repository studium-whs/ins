package de.joelag.feedbackevaluator.controller;

import de.joelag.feedbackevaluator.domain.FeedbackFormDetails;
import de.joelag.feedbackevaluator.domain.FeedbackFormXml;
import de.joelag.feedbackevaluator.dto.StatisticsDto;
import de.joelag.feedbackevaluator.repository.IFeedbackFormDetailRepository;
import de.joelag.feedbackevaluator.repository.IFeedbackFromXmlRepository;
import de.joelag.feedbackevaluator.saxhandler.FeedbackContentHandler;
import de.joelag.feedbackevaluator.saxhandler.FeedbackDatabaseErrorHandler;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Log4j2
@Service
public class FeedbackControllerImpl implements IFeedbackController {

    private final IFeedbackFormDetailRepository feedbackFormDetailRepository;
    private final IFeedbackFromXmlRepository feedbackFromXmlRepository;

    private XMLEvent lineBreak = null;
    private XMLEvent indent = null;

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    public FeedbackControllerImpl(IFeedbackFormDetailRepository feedbackFormDetailRepository, IFeedbackFromXmlRepository feedbackFromXmlRepository) {
        this.feedbackFormDetailRepository = feedbackFormDetailRepository;
        this.feedbackFromXmlRepository = feedbackFromXmlRepository;
    }

    @Override
    public void uploadFeedbackForm(MultipartFile multipartFile) {

        File file = null;

        try {
            File tempFileDir = File.createTempFile("tmp_", LocalDateTime.now().toString());
            file = new File(tempFileDir.getAbsolutePath());

            multipartFile.transferTo(file);
        } catch (IOException e) {
            log.error("Failed to create temporary directory.", e);
        }

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        List<?> parsedFiles = entityManager.createQuery("select filename from FEEDBACK_FORM_XML").getResultList();

        if (file != null && !parsedFiles.contains(file.getName())) {
            if (parseFeedback(file)) {
                log.info("Successfully parsed feedback file: {}", file.getName());
            } else {
                log.error("Failed to parse feedback file: {}", file.getName());
            }
        }
    }

    @Override
    public void evaluateFormsInDirectory(String directory) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        List<?> parsedFiles = entityManager.createQuery("select filename from FEEDBACK_FORM_XML").getResultList();

        File scanDir = new File(directory);
        String[] fileExtension = {"xml"};

        Collection<File> fileList = FileUtils.listFiles(scanDir, fileExtension, true);

        fileList.removeIf(file -> parsedFiles.contains(file.getName()));

        for (File file : fileList) {
            if (parseFeedback(file)) {
                log.info("Successfully parsed feedback file: {}", file.getName());
            } else {
                log.warn("Failed to parse feedback file: {}", file.getName());
            }
        }
    }

    @Override
    public String validateFeedbackDatabase() {

        boolean success = true;

        File file = new File("./xml/generated/accumulated/feedback_database.xml");

        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        saxParserFactory.setValidating(true);
        try {

            SAXParser saxParser = saxParserFactory.newSAXParser();

            /* set these parameters to ensure secure xml parsing */
            saxParser.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            saxParser.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

            FeedbackDatabaseErrorHandler feedbackDatabaseErrorHandler = new FeedbackDatabaseErrorHandler();
            saxParser.parse(file, feedbackDatabaseErrorHandler);

            log.info("File: {}, is valid according to it's DTD specification.", file.getName());

        } catch (ParserConfigurationException | SAXException | IOException e) {
            log.error("Failed to parse file: {}.", file.getName(), e);
            success = false;
        }

        if (success) {
            return "The database file is valid.";
        } else {
            return "The database file is invalid. Check the application log for details.";
        }
    }

    @Override
    public void generateAccumulatedXml() {
        XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
        XMLEventFactory xmlEventFactory = XMLEventFactory.newInstance();

        lineBreak = xmlEventFactory.createDTD("\n");
        indent = xmlEventFactory.createDTD("\t");

        try {
            XMLEventWriter xmlEventWriter = xmlOutputFactory.createXMLEventWriter(new FileOutputStream("./xml/generated/accumulated/feedback_database.xml"));

            createDocument(xmlEventWriter, xmlEventFactory);

        } catch (XMLStreamException | FileNotFoundException e) {
            log.error("Failed to create xml file.", e);
        }
    }

    @Override
    public StatisticsDto getStatistics() {
        StatisticsDto statisticsDto = new StatisticsDto();

        List<FeedbackFormDetails> feedbackFormDetailsList = feedbackFormDetailRepository.findAll();

        statisticsDto.setAverageLayoutRating(calculateAverageLayoutRating(feedbackFormDetailsList));
        statisticsDto.setRevisitPercentage(calculateRevisitPercentage(feedbackFormDetailsList));
        statisticsDto.setMailPercentage(calculateMailPercentage(feedbackFormDetailsList));

        log.info("Durchschnittliche Bewertung des Aussehens: {}", statisticsDto.getAverageLayoutRating());
        log.info("Anteil der Wiederkehrenden Besucher: {} %", statisticsDto.getRevisitPercentage());
        log.info("Anteil der Besucher die eine Email angefragt haben: {} %", statisticsDto.getMailPercentage());

        return statisticsDto;
    }

    private double calculateAverageLayoutRating(List<FeedbackFormDetails> feedbackFormDetailsList) {
        return feedbackFormDetailsList
                .stream()
                .mapToDouble(FeedbackFormDetails::getAppearanceRating).sum() / feedbackFormDetailsList.size();
    }

    private double calculateRevisitPercentage(List<FeedbackFormDetails> feedbackFormDetailsList) {
        return ((feedbackFormDetailsList
                .stream()
                .filter(feedbackFormDetails -> feedbackFormDetails.getRevisit() != null && feedbackFormDetails.getRevisit())
                .count() * 1d / feedbackFormDetailsList.size()) * 100);
    }

    private double calculateMailPercentage(List<FeedbackFormDetails> feedbackFormDetailsList) {
        return ((feedbackFormDetailsList
                .stream()
                .filter(feedbackFormDetails -> feedbackFormDetails.getSendCopyPerMail() != null && feedbackFormDetails.getSendCopyPerMail())
                .count() * 1d / feedbackFormDetailsList.size()) * 100);
    }

    private boolean parseFeedback(File file) {
        boolean success = true;

        FeedbackFormDetails feedbackFormDetails = new FeedbackFormDetails();

        FeedbackFormXml feedbackFormXml = new FeedbackFormXml();
        feedbackFormXml.setFilename(file.getName());
        feedbackFormXml.setFeedbackFormDetails(feedbackFormDetails);

        try {
            feedbackFormXml.setXmlFile(FileUtils.readFileToByteArray(file));
        } catch (IOException e) {
            log.error("Failed to convert file ({}) to byte array.", file.getName(), e);
        }

        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        saxParserFactory.setValidating(true);
        try {

            SAXParser saxParser = saxParserFactory.newSAXParser();

            /* set these parameters to ensure secure xml parsing */
            saxParser.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            saxParser.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

            FeedbackContentHandler contentHandler = new FeedbackContentHandler(feedbackFormDetails);
            saxParser.parse(file, contentHandler);

            feedbackFromXmlRepository.save(feedbackFormXml);
            feedbackFormDetailRepository.save(feedbackFormDetails);

            log.info("Successfully parsed file: {}", file.getName());

        } catch (ParserConfigurationException | SAXException | IOException e) {
            success = false;
            log.error("Failed to parse file: {}.", file.getName(), e);
        }

        return success;
    }

    private void createDocument(XMLEventWriter xmlEventWriter, XMLEventFactory xmlEventFactory) throws XMLStreamException {
        StartDocument startDocument = xmlEventFactory.createStartDocument("UTF-8");
        xmlEventWriter.add(startDocument);
        xmlEventWriter.add(lineBreak);

        createDtd(xmlEventWriter, xmlEventFactory);

        StartElement feedbackDatabaseStart = xmlEventFactory.createStartElement("", "", "feedbackdatenbank");
        xmlEventWriter.add(feedbackDatabaseStart);

        xmlEventWriter.add(lineBreak);

        List<FeedbackFormDetails> feedbackFormDetailsList = feedbackFormDetailRepository.findAll();

        feedbackFormDetailsList.forEach(feedbackFormDetails -> {
            try {
                createFeedbackNode(xmlEventWriter, xmlEventFactory, feedbackFormDetails);
            } catch (XMLStreamException e) {
                log.error("Failed to create feedback element for feedback form: {}", feedbackFormDetails.getId(), e);
            }
        });

        StartElement parserNameStart = xmlEventFactory.createStartElement("", "", "entwickler_parser");
        xmlEventWriter.add(parserNameStart);

        Characters characters = xmlEventFactory.createCharacters("INS Parser von Joel Lagerwall");
        xmlEventWriter.add(characters);

        EndElement parserNameEnd = xmlEventFactory.createEndElement("", "", "entwickler_parser");
        xmlEventWriter.add(parserNameEnd);

        xmlEventWriter.add(lineBreak);

        EndElement feedbackDatabaseEnd = xmlEventFactory.createEndElement("", "", "feedbackdatenbank");
        xmlEventWriter.add(feedbackDatabaseEnd);

        xmlEventWriter.add(lineBreak);

        createDocumentEnd(xmlEventWriter);
    }

    private void createFeedbackNode(XMLEventWriter xmlEventWriter, XMLEventFactory xmlEventFactory, FeedbackFormDetails feedbackFormDetails) throws XMLStreamException {

        xmlEventWriter.add(indent);

        StartElement feedbackStart = xmlEventFactory.createStartElement("", "", "feedback");
        xmlEventWriter.add(feedbackStart);
        xmlEventWriter.add(lineBreak);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);

        createVisitor(xmlEventWriter, xmlEventFactory, feedbackFormDetails);

        createRating(xmlEventWriter, xmlEventFactory, feedbackFormDetails);

        createInfo(xmlEventWriter, xmlEventFactory, feedbackFormDetails);

        EndElement feedbackEnd = xmlEventFactory.createEndElement("", "", "feedback");
        xmlEventWriter.add(feedbackEnd);

        xmlEventWriter.add(lineBreak);
    }

    private void createVisitor(XMLEventWriter xmlEventWriter, XMLEventFactory xmlEventFactory, FeedbackFormDetails feedbackFormDetails) throws XMLStreamException {
        StartElement visitorStart = xmlEventFactory.createStartElement("", "", "besucher");
        xmlEventWriter.add(visitorStart);

        List<Attribute> visitorAttributes = new ArrayList<>();
        visitorAttributes.add(xmlEventFactory.createAttribute("anrede", convertFormOfAddress(feedbackFormDetails.getFormOfAddress())));
        visitorAttributes.add(xmlEventFactory.createAttribute("vorname", feedbackFormDetails.getFirstName()));
        visitorAttributes.add(xmlEventFactory.createAttribute("nachname", feedbackFormDetails.getLastName()));

        for (Attribute attribute : visitorAttributes) {
            xmlEventWriter.add(attribute);
        }

        xmlEventWriter.add(lineBreak);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);

        StartElement ageStart = xmlEventFactory.createStartElement("", "", "alter");
        xmlEventWriter.add(ageStart);

        Characters ageValue = xmlEventFactory.createCharacters(feedbackFormDetails.getAge().toString());
        xmlEventWriter.add(ageValue);

        EndElement ageEnd = xmlEventFactory.createEndElement("", "", "alter");
        xmlEventWriter.add(ageEnd);

        xmlEventWriter.add(lineBreak);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);


        createContact(xmlEventWriter, xmlEventFactory, feedbackFormDetails);


        EndElement visitorEnd = xmlEventFactory.createEndElement("", "", "besucher");
        xmlEventWriter.add(visitorEnd);

        xmlEventWriter.add(lineBreak);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);

    }

    private void createContact(XMLEventWriter xmlEventWriter, XMLEventFactory xmlEventFactory, FeedbackFormDetails feedbackFormDetails) throws XMLStreamException {
        StartElement contactStart = xmlEventFactory.createStartElement("", "", "kontakt");
        xmlEventWriter.add(contactStart);

        Attribute callbacks = xmlEventFactory.createAttribute("rueckfrage_erlaubt", feedbackFormDetails.getCallbacks() == null ? "false" : feedbackFormDetails.getCallbacks().toString());
        xmlEventWriter.add(callbacks);

        if (feedbackFormDetails.getEmailAddress() != null) {
            xmlEventWriter.add(lineBreak);
            xmlEventWriter.add(indent);
            xmlEventWriter.add(indent);
            xmlEventWriter.add(indent);
            xmlEventWriter.add(indent);

            StartElement emailAddressStart = xmlEventFactory.createStartElement("", "", "emailadresse");
            xmlEventWriter.add(emailAddressStart);

            Characters emailValue = xmlEventFactory.createCharacters(feedbackFormDetails.getEmailAddress());
            xmlEventWriter.add(emailValue);

            EndElement emailAddressEnd = xmlEventFactory.createEndElement("", "", "emailadresse");
            xmlEventWriter.add(emailAddressEnd);
        }

        if (feedbackFormDetails.getUrl() != null) {
            xmlEventWriter.add(lineBreak);
            xmlEventWriter.add(indent);
            xmlEventWriter.add(indent);
            xmlEventWriter.add(indent);
            xmlEventWriter.add(indent);

            StartElement urlStart = xmlEventFactory.createStartElement("", "", "website");
            xmlEventWriter.add(urlStart);

            Characters urlValue = xmlEventFactory.createCharacters(feedbackFormDetails.getUrl());
            xmlEventWriter.add(urlValue);

            EndElement urlEnd = xmlEventFactory.createEndElement("", "", "website");
            xmlEventWriter.add(urlEnd);
        }

        if (feedbackFormDetails.getPhoneNumber() != null) {
            xmlEventWriter.add(lineBreak);
            xmlEventWriter.add(indent);
            xmlEventWriter.add(indent);
            xmlEventWriter.add(indent);
            xmlEventWriter.add(indent);

            StartElement phoneNumberStart = xmlEventFactory.createStartElement("", "", "telefonnummer");
            xmlEventWriter.add(phoneNumberStart);

            Characters phoneNumberValue = xmlEventFactory.createCharacters(feedbackFormDetails.getPhoneNumber());
            xmlEventWriter.add(phoneNumberValue);

            EndElement phoneNumberEnd = xmlEventFactory.createEndElement("", "", "telefonnummer");
            xmlEventWriter.add(phoneNumberEnd);

        }

        xmlEventWriter.add(lineBreak);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);


        EndElement contactEnd = xmlEventFactory.createEndElement("", "", "kontakt");
        xmlEventWriter.add(contactEnd);

        xmlEventWriter.add(lineBreak);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);
    }

    private void createRating(XMLEventWriter xmlEventWriter, XMLEventFactory xmlEventFactory, FeedbackFormDetails feedbackFormDetails) throws XMLStreamException {
        StartElement ratingStart = xmlEventFactory.createStartElement("", "", "bewertung");
        xmlEventWriter.add(ratingStart);

        List<Attribute> ratingAttributes = new ArrayList<>();
        ratingAttributes.add(xmlEventFactory.createAttribute("erneuter_besuch", convertCheckbox(feedbackFormDetails.getRevisit())));
        ratingAttributes.add(xmlEventFactory.createAttribute("note_inhalt", convertContentRating(feedbackFormDetails.getContentRating())));
        ratingAttributes.add(xmlEventFactory.createAttribute("note_aussehen", feedbackFormDetails.getAppearanceRating() == null ? "keine_bewertung" : feedbackFormDetails.getAppearanceRating().toString()));

        for (Attribute attribute : ratingAttributes) {
            xmlEventWriter.add(attribute);
        }

        xmlEventWriter.add(lineBreak);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);

        StartElement improvementsStart = xmlEventFactory.createStartElement("", "", "vorschlag");
        xmlEventWriter.add(improvementsStart);

        Characters improvementsValue = xmlEventFactory.createCharacters(feedbackFormDetails.getImprovements());
        xmlEventWriter.add(improvementsValue);

        EndElement improvementsEnd = xmlEventFactory.createEndElement("", "", "vorschlag");
        xmlEventWriter.add(improvementsEnd);

        xmlEventWriter.add(lineBreak);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);

        EndElement ratingEnd = xmlEventFactory.createEndElement("", "", "bewertung");
        xmlEventWriter.add(ratingEnd);

        xmlEventWriter.add(lineBreak);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);
    }

    private void createInfo(XMLEventWriter xmlEventWriter, XMLEventFactory xmlEventFactory, FeedbackFormDetails feedbackFormDetails) throws XMLStreamException {
        StartElement infoStart = xmlEventFactory.createStartElement("", "", "info");
        xmlEventWriter.add(infoStart);
        xmlEventWriter.add(lineBreak);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);

        if (feedbackFormDetails.getSendCopyPerMail() != null) {
            StartElement sendCopyPerMailStart = xmlEventFactory.createStartElement("", "", "email-gesendet");
            xmlEventWriter.add(sendCopyPerMailStart);

            Characters sendCopyPerMailValue = xmlEventFactory.createCharacters(convertCheckbox(feedbackFormDetails.getSendCopyPerMail()));
            xmlEventWriter.add(sendCopyPerMailValue);

            EndElement sendCopyPerMailEnd = xmlEventFactory.createEndElement("", "", "email-gesendet");
            xmlEventWriter.add(sendCopyPerMailEnd);

            xmlEventWriter.add(lineBreak);
            xmlEventWriter.add(indent);
            xmlEventWriter.add(indent);
            xmlEventWriter.add(indent);
        }

        StartElement dateStart = xmlEventFactory.createStartElement("", "", "datum");
        xmlEventWriter.add(dateStart);

        Characters dateValue = xmlEventFactory.createCharacters(LocalDate.from(feedbackFormDetails.getXmlCreationTimestamp()).toString());
        xmlEventWriter.add(dateValue);

        EndElement dateEnd = xmlEventFactory.createEndElement("", "", "datum");
        xmlEventWriter.add(dateEnd);

        xmlEventWriter.add(lineBreak);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);


        StartElement timeStart = xmlEventFactory.createStartElement("", "", "uhrzeit");
        xmlEventWriter.add(timeStart);

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

        Characters timeValue = xmlEventFactory.createCharacters(dateTimeFormatter.format(feedbackFormDetails.getXmlCreationTimestamp()));
        xmlEventWriter.add(timeValue);

        EndElement timeEnd = xmlEventFactory.createEndElement("", "", "uhrzeit");
        xmlEventWriter.add(timeEnd);

        xmlEventWriter.add(lineBreak);
        xmlEventWriter.add(indent);
        xmlEventWriter.add(indent);

        EndElement infoEnd = xmlEventFactory.createEndElement("", "", "info");
        xmlEventWriter.add(infoEnd);

        xmlEventWriter.add(lineBreak);
        xmlEventWriter.add(indent);
    }

    private void createDocumentEnd(XMLEventWriter xmlEventWriter) throws XMLStreamException {
        XMLEventFactory xmlEventFactory = XMLEventFactory.newInstance();

        EndDocument endDocument = xmlEventFactory.createEndDocument();
        xmlEventWriter.add(endDocument);
    }

    private void createDtd(XMLEventWriter xmlEventWriter, XMLEventFactory xmlEventFactory) throws XMLStreamException {
        DTD dtd = xmlEventFactory.createDTD("""
                <!DOCTYPE feedbackdatenbank [
                                  <!ELEMENT feedbackdatenbank (feedback*, entwickler_parser)>
                                  <!ELEMENT feedback (besucher,bewertung,info)>
                                  <!ELEMENT besucher (alter,kontakt)>
                                  <!ATTLIST besucher
                                          anrede (Herr|Frau|Doktor|Professor) #IMPLIED
                                          vorname CDATA #REQUIRED
                                          nachname CDATA #REQUIRED>
                                  <!ELEMENT kontakt (emailadresse?,website?,telefonnummer?)>
                                  <!ATTLIST kontakt
                                          rueckfrage_erlaubt (true|false) #IMPLIED>
                                  <!ELEMENT alter (#PCDATA)>
                                  <!ELEMENT emailadresse (#PCDATA)>
                                  <!ELEMENT telefonnummer (#PCDATA)>
                                  <!ELEMENT website (#PCDATA)>
                                  <!ELEMENT bewertung (vorschlag?)>
                                  <!ATTLIST bewertung
                                          erneuter_besuch (ja|nein) "ja"
                                          note_inhalt (sehr_gut|gut|befriedigend|ausreichend|mangelhaft|ungenuegend) #IMPLIED
                                          note_aussehen (1|2|3|4|5|6) #IMPLIED>
                                  <!ELEMENT vorschlag (#PCDATA)>
                                  <!ELEMENT info (email-gesendet?,datum,uhrzeit)>
                                  <!ELEMENT email-gesendet (#PCDATA)>
                                  <!ELEMENT datum (#PCDATA)>
                                  <!ELEMENT uhrzeit (#PCDATA)>
                                  <!ELEMENT entwickler_parser (#PCDATA)>
                                  ]>""");

        xmlEventWriter.add(dtd);
        xmlEventWriter.add(lineBreak);
    }

    private String convertFormOfAddress(String formOfAddress) {
        return switch (formOfAddress) {
            case "Dr." -> "Doktor";
            case "Prof." -> "Professor";
            default -> formOfAddress;
        };
    }

    private String convertCheckbox(Boolean checkboxValue) {
        if (checkboxValue == null) {
            return "nein";
        } else if (checkboxValue) {
            return "ja";
        } else {
            return "nein";
        }
    }

    private String convertContentRating(Integer contentRating) {
        return switch (contentRating) {
            case 1 -> "sehr_gut";
            case 2 -> "gut";
            case 3 -> "befridigend";
            case 4 -> "ausreichend";
            case 5 -> "mangelhaft";
            case 6 -> "ungenuegend";
            default -> "keine_bewertung";
        };
    }
}
