package de.joelag.feedbackevaluator.saxhandler;

import lombok.extern.log4j.Log4j2;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

@Log4j2
public class FeedbackDatabaseErrorHandler extends DefaultHandler {

    @Override
    public void warning(SAXParseException e) throws SAXException {
        log.warn("A warning occurred while validating the xml file.", e);
        throw new SAXException(e);
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        log.error("An error occurred while validating the xml file.", e);
        throw new SAXException(e);
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        log.warn("A fatal error occurred while validating the xml file.", e);
        throw new SAXException(e);
    }
}
