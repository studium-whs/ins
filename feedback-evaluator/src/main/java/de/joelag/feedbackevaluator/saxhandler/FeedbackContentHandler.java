package de.joelag.feedbackevaluator.saxhandler;

import de.joelag.feedbackevaluator.domain.FeedbackFormDetails;
import lombok.Data;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
public class FeedbackContentHandler extends DefaultHandler {

    private boolean personalInformation;
    private boolean formOfAddress;
    private boolean firstName;
    private boolean lastName;
    private boolean age;
    private boolean emailAddress;
    private boolean phoneNumber;
    private boolean url;

    private boolean feedbackDetails;
    private boolean revisit;
    private boolean improvements;
    private boolean attentive;
    private boolean contentRating;
    private boolean appearanceRating;

    private boolean other;
    private boolean sendCopyPerMail;
    private boolean callbacks;
    private boolean password;
    private boolean xmlCreationTimestamp;

    private FeedbackFormDetails feedbackFormDetails;

    public FeedbackContentHandler(FeedbackFormDetails feedbackFormDetails) {
        this.feedbackFormDetails = feedbackFormDetails;
    }

    @Override
    public void startDocument() {
        setAllFieldsToFalse();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        switch (qName) {
            case "personalInformation" -> personalInformation = true;
            case "formOfAddress" -> formOfAddress = true;
            case "firstName" -> firstName = true;
            case "lastName" -> lastName = true;
            case "age" -> age = true;
            case "emailAddress" -> emailAddress = true;
            case "phoneNumber" -> phoneNumber = true;
            case "url" -> url = true;
            case "feedbackDetails" -> feedbackDetails = true;
            case "revisit" -> revisit = true;
            case "improvements" -> improvements = true;
            case "attentive" -> attentive = true;
            case "contentRating" -> contentRating = true;
            case "appearanceRating" -> appearanceRating = true;
            case "other" -> other = true;
            case "sendCopyPerMail" -> sendCopyPerMail = true;
            case "callbacks" -> callbacks = true;
            case "password" -> password = true;
            case "creationTimestamp" -> xmlCreationTimestamp= true;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        if (personalInformation) {
            if (formOfAddress) {
                feedbackFormDetails.setFormOfAddress(new String(ch, start, length));
            }

            if (firstName) {
                feedbackFormDetails.setFirstName(new String(ch, start, length));
            }

            if (lastName) {
                feedbackFormDetails.setLastName(new String(ch, start, length));
            }

            if (age) {
                feedbackFormDetails.setAge(Integer.valueOf(new String(ch, start, length)));
            }

            if (emailAddress) {
                feedbackFormDetails.setEmailAddress(new String(ch, start, length));
            }

            if (phoneNumber) {
                feedbackFormDetails.setPhoneNumber(new String(ch, start, length));
            }

            if (url) {
                feedbackFormDetails.setUrl(new String(ch, start, length));
            }
        }

        if (feedbackDetails) {
            if (revisit) {
                String revisit = new String(ch, start, length);

                feedbackFormDetails.setRevisit("1".equalsIgnoreCase(revisit));
            }

            if (improvements) {
                feedbackFormDetails.setImprovements(feedbackFormDetails.getImprovements() == null
                        ? "" + new String(ch, start, length) :feedbackFormDetails.getImprovements()
                        + new String(ch, start, length));
            }

            if (attentive) {
                feedbackFormDetails.setAttentive(new String(ch, start, length));
            }

            if (contentRating) {
                feedbackFormDetails.setContentRating(Integer.valueOf(new String(ch, start, length)));
            }

            if (appearanceRating) {
                feedbackFormDetails.setAppearanceRating(Integer.valueOf(new String(ch, start, length)));
            }
        }

        if (other) {
            if (sendCopyPerMail) {
                String sendCopyPerMail = new String(ch, start, length);

                feedbackFormDetails.setSendCopyPerMail("1".equalsIgnoreCase(sendCopyPerMail));
            }

            if (callbacks) {
                String callbacks = new String(ch, start, length);

                feedbackFormDetails.setCallbacks("1".equalsIgnoreCase(callbacks));
            }

            if (password) {
                feedbackFormDetails.setPassword(new String(ch, start, length));
            }

            if (xmlCreationTimestamp) {
                feedbackFormDetails.setXmlCreationTimestamp(LocalDateTime.parse(new String(ch, start, length),
                        DateTimeFormatter.ISO_OFFSET_DATE_TIME));
            }
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        switch (qName) {
            case "personalInformation" -> personalInformation = false;
            case "formOfAddress" -> formOfAddress = false;
            case "firstName" -> firstName = false;
            case "lastName" -> lastName = false;
            case "age" -> age = false;
            case "emailAddress" -> emailAddress = false;
            case "phoneNumber" -> phoneNumber = false;
            case "url" -> url = false;
            case "feedbackDetails" -> feedbackDetails = false;
            case "revisit" -> revisit = false;
            case "improvements" -> improvements = false;
            case "attentive" -> attentive = false;
            case "contentRating" -> contentRating = false;
            case "appearanceRating" -> appearanceRating = false;
            case "other" -> other = false;
            case "sendCopyPerMail" -> sendCopyPerMail = false;
            case "callbacks" -> callbacks = false;
            case "password" -> password = false;
            case "creationTimestamp" -> xmlCreationTimestamp= false;
        }
    }

    @Override
    public void endDocument() {
        setAllFieldsToFalse();
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        throw new SAXException(e);
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        throw new SAXException(e);
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        throw new SAXException(e);
    }

    private void setAllFieldsToFalse() {
        personalInformation = false;
        formOfAddress = false;
        firstName = false;
        lastName = false;
        age = false;
        emailAddress = false;
        phoneNumber = false;
        url = false;
        feedbackDetails = false;
        revisit = false;
        improvements = false;
        attentive = false;
        contentRating = false;
        appearanceRating = false;
        other = false;
        sendCopyPerMail = false;
        callbacks = false;
        password = false;
        xmlCreationTimestamp = false;
    }
}
